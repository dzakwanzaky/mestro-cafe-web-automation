<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_minus</name>
   <tag></tag>
   <elementGuidId>810f72b6-400a-4134-bdbc-d6336cc8ebab</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='__next']/div/div[3]/div/div/div/div/div/div[2]/button/div/p</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>p.MuiTypography-root.MuiTypography-body1.css-1ui9mtf</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
      <webElementGuid>d98f459c-f700-457c-90ea-7be963888a56</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MuiTypography-root MuiTypography-body1 css-1ui9mtf</value>
      <webElementGuid>03ddf8dd-1a85-42ce-ac4c-3ffac54af9f4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>-</value>
      <webElementGuid>a945137f-3e81-4c0a-aacc-42acb6e94910</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;__next&quot;)/div[@class=&quot;css-1ixgbcy&quot;]/div[@class=&quot;css-yvopwm&quot;]/div[@class=&quot;css-19ec3x4&quot;]/div[@class=&quot;css-1773by3&quot;]/div[@class=&quot;css-127bjm&quot;]/div[@class=&quot;css-56jipt&quot;]/div[@class=&quot;css-1kze96t&quot;]/div[@class=&quot;css-14l31q&quot;]/button[@class=&quot;MuiButton-root MuiButton-contained MuiButton-containedPrimary MuiButton-sizeMedium MuiButton-containedSizeMedium MuiButtonBase-root  css-sz459u&quot;]/div[@class=&quot;MuiBox-root css-sjx0zd&quot;]/p[@class=&quot;MuiTypography-root MuiTypography-body1 css-1ui9mtf&quot;]</value>
      <webElementGuid>4b84f9ce-a229-4a91-85ad-df7b9c610d6c</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='__next']/div/div[3]/div/div/div/div/div/div[2]/button/div/p</value>
      <webElementGuid>b5fc10b3-7920-4b6a-abac-4f4388f5f3f0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Capture object:'])[1]/preceding::p[5]</value>
      <webElementGuid>92ade5bc-4ce4-4736-be8d-4b6be5cc818c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Alt'])[1]/preceding::p[5]</value>
      <webElementGuid>e5d4d765-1a7c-437e-8d38-cc1aaa1d5540</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='-']/parent::*</value>
      <webElementGuid>457b8f62-5709-40cc-860d-1200d19d427d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//button/div/p</value>
      <webElementGuid>639fe850-ae35-40af-8696-8340498e737a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//p[(text() = '-' or . = '-')]</value>
      <webElementGuid>5931a9aa-98db-44b3-b0d0-38c80eedb596</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
