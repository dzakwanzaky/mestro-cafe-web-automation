<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>product_detail_name</name>
   <tag></tag>
   <elementGuidId>4553c847-0f76-4be5-aa5c-df673a13a4f7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='`'])[2]/following::p[1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>p.MuiTypography-root.MuiTypography-body1.css-rghnp1</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
      <webElementGuid>87be43b3-3279-446e-bf66-2b2ec6392766</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MuiTypography-root MuiTypography-body1 css-rghnp1</value>
      <webElementGuid>66adfa8f-b2be-4411-93dd-d0d197418dde</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Test Coba 123 Variant 1: Biru</value>
      <webElementGuid>5f53244c-1a7b-4f9d-b554-0a0317a887f7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;MuiModal-root MuiDialog-root css-126xj0f&quot;]/div[@class=&quot;MuiDialog-container MuiDialog-scrollPaper css-ekeie0&quot;]/div[@class=&quot;MuiPaper-root MuiPaper-elevation MuiPaper-rounded MuiPaper-elevation24 MuiDialog-paper MuiDialog-paperScrollPaper MuiDialog-paperWidthSm MuiDialog-paperFullScreen css-1hyl1h2&quot;]/div[@class=&quot;css-1wlbyqk&quot;]/div[@class=&quot;css-1jiq011&quot;]/p[@class=&quot;MuiTypography-root MuiTypography-body1 css-rghnp1&quot;]</value>
      <webElementGuid>9acfdd38-d014-4bfe-8763-eeae78b467a9</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='`'])[2]/following::p[1]</value>
      <webElementGuid>e9ed78fa-d0f1-471c-9550-e296af944383</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Alt'])[2]/following::p[1]</value>
      <webElementGuid>86da0615-7d36-45ce-93b3-6f67e167c6af</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='​'])[1]/preceding::p[2]</value>
      <webElementGuid>e9698de6-1df1-4779-9636-5a5d98136cda</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/p</value>
      <webElementGuid>d68948bf-929f-4fd3-a564-33cb57e6f808</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//p[(text() = 'Test Coba 123 Variant 1: Biru' or . = 'Test Coba 123 Variant 1: Biru')]</value>
      <webElementGuid>e3dc2979-1972-4805-8525-4db94822383b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
