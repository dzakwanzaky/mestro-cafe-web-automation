package stepDefinition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import cucumber.api.java.en.Given
import cucumber.api.java.en.When
import cucumber.api.java.en.Then
import internal.GlobalVariable

public class Product {
	@Given("user is in Homepage")
	public void user_is_in_Homepage() {
		WebUI.openBrowser('https://wetwo.id/')
		WebUI.waitForElementPresent(findTestObject('Homepage/btn_toko'), 0)
		WebUI.verifyElementPresent(findTestObject('Homepage/btn_toko'), 0)
	}
	
	@When("user select one of available location")
	public void user_select_one_of_available_location() {
		WebUI.click(findTestObject('Object Repository/Homepage/btn_toko'))
	}
	
	@When("user add add product to cart and add the qty")
	public void user_add_add_product_to_cart_and_add_the_qty() {
		WebUI.click(findTestObject('Homepage/btn_tambah'))
		WebUI.click(findTestObject('Homepage/btn_add'))
		WebUI.waitForElementPresent(findTestObject('Homepage/btn_cart'), 0)
	}
	
	@When("user go to the Cart page and reduce the qty")
	public void user_go_to_the_Cart_page_and_reduce_the_qty() {
		WebUI.click(findTestObject('Homepage/btn_cart'))
		WebUI.waitForElementPresent(findTestObject('Cart Page/product_name'), 0)
		WebUI.verifyElementText(findTestObject('Cart Page/product_name'), 'Test Coba 123 Variant 1: Biru')
		WebUI.click(findTestObject('Cart Page/btn_minus'))
	}
	
	@When("user click the product image")
	public void user_click_the_product_image() {
		WebUI.click(findTestObject('Cart Page/img_product'))
	}
	
	@Then("user can go to the product detail")
	public void user_can_go_to_the_product_detail() {
		WebUI.verifyElementPresent(findTestObject('Product Detail Page/img_product_detail'), 0)
		WebUI.verifyElementPresent(findTestObject('Product Detail Page/product_detail_name'), 0)
		WebUI.verifyElementPresent(findTestObject('Product Detail Page/btn_add_to_cart'), 0)
		WebUI.closeBrowser()
	}
}
