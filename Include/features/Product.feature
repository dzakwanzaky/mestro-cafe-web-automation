Feature: Product Feature

  Scenario: User can add to cart, reduce cart qty, and go to product detail
    Given user is in Homepage
    When user select one of available location
    And user add add product to cart and add the qty
    And user go to the Cart page and reduce the qty
    And user click the product image
    Then user can go to the product detail
