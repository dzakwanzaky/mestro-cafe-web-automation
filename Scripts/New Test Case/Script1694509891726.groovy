import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import java.awt.AWTException as AWTException
import java.awt.Robot as Robot
import java.awt.event.KeyEvent as KeyEvent
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver

WebUI.openBrowser('https://wetwo.id/')

WebUI.waitForElementPresent(findTestObject('Homepage/btn_toko'), 0)

WebUI.click(findTestObject('Object Repository/Homepage/btn_toko'))

WebUI.waitForElementPresent(findTestObject('Homepage/btn_tambah'), 0)

WebUI.click(findTestObject('Homepage/btn_tambah'))

WebUI.click(findTestObject('Homepage/btn_add'))

WebUI.waitForElementPresent(findTestObject('Homepage/btn_cart'), 0)

WebUI.click(findTestObject('Homepage/btn_cart'))

WebUI.waitForElementPresent(findTestObject('Cart Page/product_name'), 0)

WebUI.verifyElementText(findTestObject('Cart Page/product_name'), 'Test Coba 123 Variant 1: Biru')

WebUI.click(findTestObject('Cart Page/btn_minus'))

WebUI.click(findTestObject('Cart Page/img_product'))

WebUI.verifyElementPresent(findTestObject('Product Detail Page/img_product_detail'), 0)

WebUI.verifyElementPresent(findTestObject('Product Detail Page/product_detail_name'), 0)

WebUI.verifyElementPresent(findTestObject('Product Detail Page/btn_add_to_cart'), 0)

WebUI.closeBrowser()

